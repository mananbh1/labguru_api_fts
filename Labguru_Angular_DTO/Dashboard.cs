﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{
   
    public class Dashboard
    {
        [key]
        [NotMapped]
        
      
        public int ProductID { get; set; }
        public string Product { get; set; }

        [NotMapped]
        public List<DashboardOutputDTO> DashboardOutputDTO { get; set; }
    }

    public class DashboardOutputDTO
    {
        public int Open { get; set; }
        public int InProcess { get; set; }
        public int Completed { get; set; }
        public int Total { get; set; }

    }
    public class DashboardInputDTO
    {

        public int UserID { get; set; }
        public int ProductID { get; set; }
        public int ProcessID { get; set; }


    }
    public class FileEntrySearchInputDTO
    {

        public int ProductID { get; set; }
        public string ImpressionNo { get; set; }
        public int ProcessID { get; set; }
        public string TransactionNumber { get; set; }
        public int SituationID { get; set; }
        public string StatusID { get; set; }
        public int UserID { get; set; }


    }
    public class FileEntrySearchOutPutDTO
    {
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public string ImpressionNo { get; set; }
        public string Product { get; set; }
        public string TrayColor { get; set; }
        public bool IsPrime { get; set; }
        public int Units { get; set; }
        public string Shade { get; set; }
        public string RedoImpressionNo { get; set; }
        public string Remark { get; set; }
        public int StatusID { get; set; }
        public string Status { get; set; }
        public string LastActivityBy { get; set; }
        public string Ageing { get; set; }
        public int Files { get; set; }
        public int ProductID { get; set; }
        public int ProcessID { get; set; }
        public int FileEntryID { get; set; }
        public int ActionID { get; set; }

     



    }
    public class FileEntryFileOutPutDTO
    {

        public string FileName { get; set; }
        public string VirtualFilePath { get; set; }
      
    }

    public class DownloadFileDetails
    {
        public string TransactionNumber { get; set; }
        public int ProcessID { get; set; }
    }
    public class SearchCaseInputDTO
    {
        public string ImpressionNo { get; set; }
    }
    public class SearchCaseOutPutDTO
    {
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }

        public string ImpressionNo { get; set; }
        public string Product { get; set; }
        public string Process { get; set; }
        public string Status { get; set; }
        public string Ageing { get; set; }
        public string Action { get; set; }
        public bool Download { get; set; }
        public bool Upload { get; set; }
        public string LastActivityBy { get; set; }
        public string LastActivityDate { get; set; }

        public string OrderFileURL { get; set; }
        public string TotalAgeing { get; set; }

    }

    public class DashboardStatusOutputDTO
    {

        public string ImpressionNo { get; set; }
        public string Product { get; set; }
        public int Units { get; set; }

        public string TrayColor { get; set; }
        public string Ageing { get; set; }
        public string TransactionNumber { get; set; }
        public string Status { get; set; }
    }


}
