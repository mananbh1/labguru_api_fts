﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MProcess", Schema = "dbo")]
   public class ProcessMaster
    {
        [Key]

        public int ProcessID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public int ActionID { get; set; }
        //public string Action { get; set; }


    }

    public class SelectProcessMaster
    {
        [Key]

        public int ProcessID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }
        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public bool Mapped { get; set; }
        public int ActionID { get; set; }
        public string Action { get; set; }
    }
}
