﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;
using System.Text;


namespace Labguru_Angular_DTO
{
    [Table("PP_TMillingEntry", Schema = "dbo")]
    public class MillingEntry
    {
        [key]
        public int MillingEntryID { get; set; }
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public int StatusID { get; set; }
        public int ProductGroupID { get; set; }
        public string LotNo { get; set; }
        public string BarcodeNo { get; set; }
        public string MillingFile_URL { get; set; }
        public string PrintOutFile_URL { get; set; }
        public string Remark { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }
    [Table("PP_TMillingEntryDetail", Schema = "dbo")]
    public class MillingEntryDetail
    {
        [key]
        public int MillingEntryDetailID { get; set; }
        public int MillingEntryID { get; set; }
        public string TransactionNumber { get; set; }
        public string ImpressionNo { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }
  
    public class MillingEntryInputDTO
    {
       
        public int ProductGroupID { get; set; }
       
        public string ImpressionNo { get; set; }
        
        public string TransactionNumber { get; set; }
        
        public int SituationID { get; set; }
        public int UserID { get; set; }

        public int? StatusID { get; set; }

    }
   
    public class MillingEntryOutPutDTO
    {
        
        public string ImpressionNo { get; set; }
        public int Units { get; set; }
        public string Shade { get; set; }
       
      
    }
    public class MillingDashboardInputDTO
    {
        public int ProcessID { get; set; }
        public int UserID { get; set; }



    }
    public class MillingDashboard
    {
        [key]
        [NotMapped]


        public int ProductGroupID { get; set; }
        public string ProductGroup { get; set; }

        [NotMapped]
        public List<MillingDashboardOutputDTO> MillingDashboardOutputDTO { get; set; }
    }

    public class MillingDashboardOutputDTO
    {
        public int Open { get; set; }
        public int InProcess { get; set; }
        public int Completed { get; set; }
        public int Total { get; set; }

    }

    public class MillingEntrySearchOutputDTO
    {

        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public string Status { get; set; }
        public string ProductGroup { get; set; }
        public string LotNo { get; set; }
        public string BarcodeNo { get; set; }
        public string PrintOutFile_URL { get; set; }
        public string MillingFile_URL { get; set; }
        public string Ageing { get; set; }
        public string Remark { get; set; }
        public string LastActivityBy { get; set; }
        public int StatusID { get; set; }
        public int ProductGroupID { get; set; }

    }
    public class MillingEntryImpressionOutputDTO
    {
        public string ImpressionNo { get; set; }
        public int Units { get; set; }
        public string Shade { get; set; }



    }
    public class UpdateMillingFileEntryInputDTO
    {

        public int UserID { get; set; }
        public int StatusID { get; set; }
        public string TransactionNumber { get; set; }
     
    }


}
