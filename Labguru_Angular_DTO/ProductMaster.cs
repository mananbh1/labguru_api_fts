﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MProduct", Schema = "dbo")]
    public class ProductMaster
    {

      
        [Key]

        public int ProductID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }

        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public int ProductGroupID { get; set; }
        //public string ProductGroup { get; set; }


    }
    public class SelectProductMaster
    {
        [Key]

        public int ProductID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }
        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public string Combine { get; set; }

        public bool Mapped { get; set; }

        public int ProductGroupID { get; set; }
        public string ProductGroup { get; set; }


    }


}
