﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{

    [Table("PP_TFileEntry", Schema = "dbo")]
    public class FileEntry
    {
        [key]
        public int FileEntryID { get; set; }
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public int StatusID { get; set; }
        public int CaseTypeID { get; set; }
        public int ScannerTypeID { get; set; }
        public int ProductID { get; set; }
        public string CaseEntryNumber { get; set; }
        public string ImpressionNo { get; set; }
        public string TrayColor { get; set; }
        public bool IsPrime { get; set; }
        public int Units { get; set; }
        public string Shade { get; set; }
        public string RedoImpressionNo { get; set; }
        public string Remark { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }

    [Table("PP_TFileEntryDetail", Schema = "dbo")]
    public  class FileEntryDetail
    {
        [key]
        public int FileEntryDetailID { get; set; }
        public int FileEntryID { get; set; }
        public int FolderID { get; set; }
        public int ProcessID { get; set; }
        public string FileName { get; set; }
        public string UploadedFileName { get; set; }
        public string VirtualFilePath { get; set; }
        public string PhysicalFilePath { get; set; }
        public string FileType { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public string TransactionNumber { get; set; }

        public string ScannedFileType { get; set; }
        public Boolean isUploaded { get; set; }
    }
    [Table("PP_TFileEntry_ProcessDetail", Schema = "dbo")]
    public class FileEntryProcessDetails
    {
        [key]
        public int ProcessDetailID { get; set; }
        public int FileEntryID { get; set; }
        public int ProductID { get; set; }
        public int ProcessID { get; set; }
        public int ActionID { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }

    public class FileEntryViewModel
    {
        [key]
        public int FileEntryID { get; set; }
        public string TransactionNumber { get; set; }
        public DateTime TransactionDate { get; set; }
        public int StatusID { get; set; }
        public int CaseTypeID { get; set; }
        public int ScannerTypeID { get; set; }
        public int ProductID { get; set; }
        public int ProcessID { get; set; }
        public string CaseEntryNumber { get; set; }
        public string ImpressionNo { get; set; }
        public string TrayColor { get; set; }
        public bool IsPrime { get; set; }
        public int Units { get; set; }
        public string Shade { get; set; }
        public string RedoImpressionNo { get; set; }
        public string Remark { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
       public List<Attachments> Attachments { get; set; }
    }


    public class Attachments
    {
        public string TransactionNumber { get; set; }
        public int FileEntryDetailID { get; set; }
        public int FolderID { get; set; }
        public string FileName { get; set; }
        public string UploadedFileName { get; set; }
        public string VirtualFilePath { get; set; }
        public string PhysicalFilePath { get; set; }
        public string FileType { get; set; }

        public string ScannedFileType { get; set; }
        public Boolean isUploaded { get; set; }


    }

    
    [Table("Server_Documents", Schema = "dbo")]
    public class ServerDocuments
    {
        [key]
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FolderPath { get; set; }
        public string AbsolutePath { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class UpdateFileEntry
    {

        public int UserID { get; set; }
        public int ProductID { get; set; }
        public int StatusID { get; set; }
        public int ProcessID { get; set; }
        public string TransactionNumber{ get; set; }
        public int SituationID { get; set; }
        public string RejectReason { get; set; }



    }
}
