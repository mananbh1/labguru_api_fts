﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MUser", Schema = "dbo")]
    public class UserMaster_Input
    {
        [Key]


        public int UserID { get; set; }

        public bool IsActive { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool FileDownloadAccess { get; set; }

        [NotMapped]

        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public int LastActivityByID { get; set; }
    }

    public class Login_Output
    {
        [Key]

        public int UserID { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool FileDownloadAccess { get; set; }
        public string Message { get; set; }
       
    }

    public class Login_Input
    {
        
        public string Code { get; set; }
        public string Password { get; set; } 

    }


    public class Menu
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int ParentMenuID { get; set; }
        public string Icon { get; set; }
        public string Collapse { get; set; }
        public string Route { get; set; }

    }

    public class UserMenu
    {
        public string MenuName { get; set; }
        public string Icon { get; set; }
        public string Collapse { get; set; }
        public string Route { get; set; }
        public List<Menu> ChildMenu { get; set; }
    }

    public class UsersMaster
    {
        //[Key]

        public int UserID { get; set; }
        public string Code { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        [NotMapped]

        public bool IsActive { get; set; }
        [NotMapped]

        public bool FileDownloadAccess { get; set; }

        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public int LastActivityByID { get; set; }
        public string UserProcessProduct { get; set; }

        public string Pages { get; set; }

    }
    public class Process
    {

        public int ProcessID { get; set; }
        public string ProcessName { get; set; }
    }
    public class ProcessProduct
    {

        public int ProcessID { get; set; }
        public string ProcessName { get; set; }

        public List<Product> Product { get; set; }



    }

    public class Product
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public bool Mapped { get; set; }

    }

    public class Page
    {
        public int PageID { get; set; }
        public bool Mapped { get; set; }
        public string pageName { get; set; }

    }
    [Table("Admin_MUserPage", Schema = "dbo")]
    public class Admin_MUserPage
    {
        [key]
        public int MappingID { get; set; }
        public int UserID { get; set; }
        public int PageID { get; set; }

    }

    public class ProductMasterData
    {
        public int ProductID { get; set; }
        public string ProductName { get; set; }

        public bool Mapped { get; set; }

    }

    public class GetUserDetail
    {

        public int UserID { get; set; }

    }


    public class GetUserDetailAdmin
    {

        [key]
        public int UserID { get; set; }

        public string Code { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool FileDownloadAccess { get; set; }

        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public int LastActivityByID { get; set; }
    }


    public class SetUsers
    {
        [Key]


        public int UserID { get; set; }

        public string Code { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool FileDownloadAccess { get; set; }

        [NotMapped]

        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public int LastActivityByID { get; set; }
    }
}



