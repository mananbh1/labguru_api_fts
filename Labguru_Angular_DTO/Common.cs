﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labguru_Angular_DTO
{
    public class CommonOutPut
    {

        public int ID { get; set; }
        public string Description { get; set; }


    }
    public class CommonInPut
    {

        public int UserID { get; set; }
        public string PageName { get; set; }


    }

    public class CommonValidationInput
    {

        public string ImpressionNo { get; set; }
        public string TransactionNo { get; set; }
        public int SituationID { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public int ProductID { get; set; }

    }


    public class CommonValidationOutput
    {
        public string Message { get; set; }
    }

}
