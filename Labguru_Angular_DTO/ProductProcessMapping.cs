﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{

    [Table("Admin_MProcessProductMapping", Schema = "dbo")]
    public class ProductProcessMapping
    {
        [KEY]
        [NotMapped]

        public int PPMapID { get; set; }

        public int ProductID { get; set; }
        public int ProcessID { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public bool IsMapped { get; set; }
    }
    public class ProductProcessMappingViewModel
    {
        public int ProductID { get; set; }

    }
    public class ProductProcessMappingSelect
    {

        [KEY]
        public int ProcessID { get; set; }
        public string Code { get; set; }
        public bool Select { get; set; }

        public string Description { get; set; }

        public string LastActivityBy { get; set; }

        public bool Mapped { get; set; }

        public int Sequence { get; set; }

        public int ActionID { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }

    public class addProductProcessMapping
    {
        [NotMapped]
        public int PPMapID { get; set; }
        public string Sequences { get; set; }

        public string Actions { get; set; }

        public int ProductID { get; set; }
        [NotMapped]
        public int ProcessID { get; set; }
        [NotMapped]
        public int Sequence { get; set; }

        public int ActionID { get; set; }


        public string Processes { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }
}
