﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Labguru_Angular_DTO
{
    public class Admin_MProduct
    {
      [KEY]
       public int ProductID { get; set; }
       public string Code { get; set; }
       public string Description { get; set; }
       public Boolean IsActivate { get; set; }
       public int CreatedByID { get; set; }
       public DateTime CreationDate { get; set; }

    }
}
