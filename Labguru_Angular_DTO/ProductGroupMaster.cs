﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MProductGroup", Schema = "dbo")]

    public class ProductGroupMaster
    {

        [Key]

        public int ProductGroupID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }

        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
    }
    public class SelectProductGroupMaster
    {
        [Key]

        public int ProductGroupID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Code { get; set; }
        [Required]
        [MaxLength(50)]
        public string Description { get; set; }

        [Required]

        public bool IsActivate { get; set; }
        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

        public string Combine { get; set; }

        public bool Mapped { get; set; }


    }

}
