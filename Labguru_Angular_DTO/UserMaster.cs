﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Labguru_Angular_DTO
{
    [Table("Admin_MUserProcessProductMapping", Schema = "dbo")]

    public class UserProductProcessMapping
    {
        
        [key]
        public int MappingID { get; set; }
        public int UserID { get; set; }

        public string ProductID { get; set; }
        public string ProcessID { get; set; }
        public int LastActivityByID { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }

    }
    public class UserMaster
    {
       
        public int UserID { get; set; }
        public string Code { get; set; }
        public string Password { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public bool FileDownloadAccess { get; set; }
        
        [NotMapped]
        public string LastActivityBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastActivityDate { get; set; }
        public int LastActivityByID { get; set; }

       
    }

    public class AllMenuShow
    {
        public int MenuID { get; set; }
        public string MenuName { get; set; }
        public int ParentMenuID { get; set; }
        public string Icon { get; set; }
        public string Collapse { get; set; }
        public string Route { get; set; }
        public int PageID { get; set; }
        public bool  Mapped { get; set; }

    }

    public class AllUserMenu
    {
        public string MenuName { get; set; }
        public string Icon { get; set; }
        public string Collapse { get; set; }
        public string Route { get; set; }
        public List<AllMenuShow> ChildMenu { get; set; }
    }


}
