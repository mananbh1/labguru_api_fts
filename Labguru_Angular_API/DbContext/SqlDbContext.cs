﻿
using Labguru_Angular_DTO;
using Microsoft.EntityFrameworkCore;

namespace Labguru_Angular_API.DbContexts
{
    public class SqlDbContext : DbContext
    {

        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {

            //Database.Migrate();
        }

            public SqlDbContext()
        {
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Data Source=.\sqlexpress;Initial Catalog=FTS;Integrated Security=SSPI;");
            //optionsBuilder.UseSqlServer(@"Data Source=192.168.125.132;Initial Catalog=FTS;Integrated Security=false;User ID=development;Password=development"); //UAT        

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductProcessMappingSelect>().HasNoKey();
            modelBuilder.Entity<ProductProcessMapping>().HasNoKey();
            //modelBuilder.Entity<UserProcessProductMapping>().HasNoKey();
            modelBuilder.Entity<ProductMasterData>().HasNoKey();
            modelBuilder.Entity<addProductProcessMapping>().HasKey(u => new { u.ProductID});
           // modelBuilder.Entity<UserMaster>().HasKey(u => new { u.UserID });
            modelBuilder.Entity<UserProductProcessMapping>().HasKey(u => new { u.MappingID });
            modelBuilder.Entity<CommonOutPut>().HasKey(u => new { u.ID });
            modelBuilder.Entity<ProcessProduct>().HasNoKey();
            modelBuilder.Entity<FileEntryProcessDetails>().HasKey(u => new { u.ProcessDetailID });
           // modelBuilder.Entity<Users>().HasNoKey();
            modelBuilder.Entity<GetUserDetailAdmin>().HasKey(u => new { u.UserID });
            modelBuilder.Entity<Dashboard>().HasNoKey();
            modelBuilder.Entity<UserMaster>().HasKey(u => new { u.UserID });

            modelBuilder.Entity<FileEntry>().HasKey(u => new { u.FileEntryID });
            modelBuilder.Entity<FileEntryDetail>().HasKey(u => new { u.FileEntryDetailID });
            modelBuilder.Entity<MillingEntry>().HasKey(u => new { u.MillingEntryID });
            modelBuilder.Entity<MillingEntryDetail>().HasKey(u => new { u.MillingEntryDetailID });
            modelBuilder.Entity<DashboardOutputDTO>().HasNoKey();
            modelBuilder.Entity<MillingDashboardOutputDTO>().HasNoKey();
            modelBuilder.Entity<MillingEntryImpressionOutputDTO>().HasNoKey();
            modelBuilder.Entity<MillingEntrySearchOutputDTO>().HasNoKey();
            modelBuilder.Entity<FileEntrySearchOutPutDTO>().HasNoKey();
            modelBuilder.Entity<FileEntryFileOutPutDTO>().HasNoKey();
            modelBuilder.Entity<SearchCaseOutPutDTO>().HasNoKey();
            modelBuilder.Entity<Page>().HasNoKey();
            modelBuilder.Entity<Menu>().HasNoKey();
            modelBuilder.Entity<Login_Output>().HasNoKey();
            modelBuilder.Entity<Admin_MUserPage>().HasKey(u => new { u.MappingID });
            modelBuilder.Entity<CommonValidationOutput>().HasNoKey();

            modelBuilder.Entity<MillingEntryOutPutDTO>().HasKey(u => new { u.ImpressionNo });

            modelBuilder.Entity<DashboardStatusOutputDTO>().HasNoKey();
            modelBuilder.Entity<AllMenuShow>().HasNoKey();
            //modelBuilder.Entity<Users>().HasKey(c => new { c.UserID });

            // mapping code Customer ==> tblCustomr
        }
        //public DbSet<Customer> Customers { get; set; }
        public DbSet<UserMaster_Input> Admin_Muser { get; set; }

        public DbSet<Login_Output> Login_Output { get; set; }
        public DbSet<Menu> Menu { get; set; }

        public DbSet<GetUserDetailAdmin> GetUserDetailAdmin { get; set; }

        public DbSet<ProductMaster> Admin_MProduct { get; set; }

        public DbSet<ProductGroupMaster> Admin_MProductGroupMaster { get; set; }
        public DbSet<ProcessMaster> Admin_MProcess { get; set; }
        public DbSet<SelectProcessMaster> SelectProcessMaster { get; set; }
        public DbSet<SelectProductMaster> SelectProductMaster { get; set; }
        public DbSet<SelectProductGroupMaster> SelectProductGroupMaster { get; set; }

        public DbSet<ProductProcessMapping> Admin_MProductProcessMapping { get; set; }

        public DbSet<ProductProcessMappingSelect> SelectAdmin_MProcessProductMapping { get; set; }
        public DbSet<addProductProcessMapping> addProcessProductMapping { get; set; }

        //public DbSet<UserMaster> InsertUserMaster { get; set; }

        public DbSet<Process> Process { get; set; }
        public DbSet<Product> Product { get; set; } 
        public DbSet<Page> Page { get; set; }
        public DbSet<ProductMasterData> ProductMasterData { get; set; }

        public DbSet<UserProductProcessMapping> Admin_MUserProcessProductMapping { get; set; }
        public DbSet<Admin_MUserPage> Admin_MUserPage { get; set; }

        public DbSet<FileEntry> PP_TFileEntry { get; set; }
        public DbSet<FileEntryDetail> PP_TFileEntryDetail { get; set; }

        public DbSet<MillingEntry> PP_TMillingEntry { get; set; }
        public DbSet<MillingEntryDetail> PP_TMillingEntryDetail { get; set; }

        public DbSet<FileEntryProcessDetails> PP_TFileEntry_ProcessDetail { get; set; }
       
        public DbSet<CommonOutPut> Common { get; set; }
        public DbSet<MillingEntryOutPutDTO> MillingEntryImpressionNo { get; set; }
        
        public DbSet<ServerDocuments> ServerDocuments { get; set; }
        public DbSet<Dashboard> Dashboards { get; set; }
        public DbSet<DashboardOutputDTO> DashboardOutputDTO { get; set; }
        public DbSet<MillingDashboardOutputDTO> MillingDashboardOutputDTO { get; set; }
        public DbSet<MillingEntrySearchOutputDTO> MillingEntrySearchOutputDTO { get; set; }

        public DbSet<MillingEntryImpressionOutputDTO> MillingEntryImpressionOutputDTO { get; set; }


        public DbSet<FileEntrySearchOutPutDTO> FileEntrySearchOutPutDTO { get; set; }
        public DbSet<FileEntryFileOutPutDTO> FileEntryFileOutPutDTO { get; set; }

        public DbSet<SearchCaseOutPutDTO> SearchCaseOutPutDTO { get; set; }

        public DbSet<Labguru_Angular_DTO.MillingEntry> MillingEntry { get; set; }

        public DbSet<CommonValidationOutput> CommonValidationOutput { get; set; }

        public DbSet<DashboardStatusOutputDTO> DashboardStatusOutputDTO { get; set; }
        public DbSet<AllMenuShow> AllMenuShow { get; set; }


        // public DbSet<UserMaster> Admin_MUser { get; set; }addProcessProductMapping

    }
}
