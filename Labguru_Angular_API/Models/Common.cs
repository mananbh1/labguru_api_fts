﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labguru_Angular_API.Models
{
    public class Common
    {
        public static Error CommonException(DbUpdateException ex)
        {
            Error obj = new Error();
            if (ex.GetBaseException().GetType() == typeof(SqlException))
            {
                Int32 ErrorCode = ((SqlException)ex.InnerException).Number;
                switch (ErrorCode)
                {
                    case 2627:
                        obj.message = "Unique constraint error";
                        obj.error = "1";
                        break;
                    case 547:
                        obj.message = "Constraint check violation";
                        obj.error = "1";
                        break;
                    case 2601:
                        obj.message = "Duplicated key row error";
                        obj.error = "1";
                        break;
                    default:
                        break;
                }
            }


            return obj;
        }
    }

    public class  Error
    {
        public string message { get; set; }
        public string error { get; set; }

    }
}
