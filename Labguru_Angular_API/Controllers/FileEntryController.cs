﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Labguru_Angular_API.Models;
using System.Web;

using System.IO;
using System.Data;
using Microsoft.Data.SqlClient;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileEntryController : ControllerBase
    {
        private readonly SqlDbContext _context;
        public FileEntryController(SqlDbContext context)
        {
            _context = context;
        }



        //[Route("[action]")]
        //[HttpPost]
        //public IActionResult SaveFileEntry(FileEntryViewModel FileEntryViewModel)
        //{
        //    Error obj = new Error();


        //    //List<FileEntry>  fileentrylist  = _context.PP_TFileEntry.ToList();
        //    //int lastFileentryID = (from f in fileentrylist select f.FileEntryID).Max();
        //    int lastFileentryID = _context.PP_TFileEntry.OrderByDescending(f => f.FileEntryID).FirstOrDefault().FileEntryID;

        //    string CurrentYear = DateTime.Now.ToString("yyyy");
        //    string varTransactionNumber = "File" + (lastFileentryID + 1) + CurrentYear;

        //    FileEntry fileEntry = new FileEntry();
        //    fileEntry.TransactionNumber = FileEntryViewModel.TransactionNumber;
        //    fileEntry.TransactionDate = FileEntryViewModel.TransactionDate;
        //    fileEntry.StatusID = FileEntryViewModel.StatusID;
        //    fileEntry.CaseTypeID = FileEntryViewModel.CaseTypeID;
        //    fileEntry.ScannerTypeID = FileEntryViewModel.ScannerTypeID;
        //    fileEntry.ProductID = FileEntryViewModel.ProductID;
        //    fileEntry.CaseEntryNumber = FileEntryViewModel.CaseEntryNumber;
        //    fileEntry.ImpressionNo = FileEntryViewModel.ImpressionNo;
        //    fileEntry.TrayColor = FileEntryViewModel.TrayColor;
        //    fileEntry.IsPrime = FileEntryViewModel.IsPrime;
        //    fileEntry.Units = FileEntryViewModel.Units;
        //    fileEntry.Shade = FileEntryViewModel.Shade;
        //    fileEntry.RedoImpressionNo = FileEntryViewModel.RedoImpressionNo;
        //    fileEntry.Remark = FileEntryViewModel.Remark;
        //    fileEntry.LastActivityByID = FileEntryViewModel.LastActivityByID;
        //    fileEntry.LastActivityDate = FileEntryViewModel.LastActivityDate;

        //    obj.message = "User Save Successfully";
        //    obj.error = "0";
        //    _context.PP_TFileEntry.Add(fileEntry);

        //    try
        //    {
        //        _context.SaveChanges();
        //    }

        //    catch (DbUpdateConcurrencyException ex)
        //    {
        //        obj = Models.Common.CommonException(ex);

        //    }
        //    catch (DbUpdateException ex)
        //    {
        //        obj = Models.Common.CommonException(ex);

        //    }

        //    //int lastuserid = User.UserID;


        //    //string str = Userviewmodel.UserProcessProduct;
        //    //List<string> str1 = str.Split(',').ToList();


        //    for (int i = 0; i < FileEntryViewModel.Attachments.Count; i++)
        //    {

        //        FileEntryDetail fileEntryDetail = new FileEntryDetail();
        //        fileEntryDetail.FileEntryID = FileEntryViewModel.FileEntryID;
        //        fileEntryDetail.LastActivityByID = FileEntryViewModel.LastActivityByID;
        //        fileEntryDetail.LastActivityDate = FileEntryViewModel.LastActivityDate;
        //        fileEntryDetail.ProcessID = FileEntryViewModel.ProcessID;


        //        fileEntryDetail.FolderID = FileEntryViewModel.Attachments[i].FolderID;
        //        fileEntryDetail.FileName = FileEntryViewModel.Attachments[i].FileName;
        //        fileEntryDetail.UploadedFileName = FileEntryViewModel.Attachments[i].UploadedFileName;
        //        fileEntryDetail.VirtualFilePath = FileEntryViewModel.Attachments[i].VirtualFilePath;
        //        fileEntryDetail.PhysicalFilePath = FileEntryViewModel.Attachments[i].PhysicalFilePath;
        //        fileEntryDetail.FileType = FileEntryViewModel.Attachments[i].FileType;


        //        _context.PP_TFileEntryDetail.Add(fileEntryDetail);
        //        try
        //        {
        //            _context.SaveChanges();
        //        }
        //        catch (DbUpdateConcurrencyException ex)
        //        {
        //            obj = Models.Common.CommonException(ex);

        //        }
        //        catch (DbUpdateException ex)
        //        {
        //            obj = Models.Common.CommonException(ex);

        //        }
        //    }



        //    string Data =
        //           "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
        //           "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
        //    return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
        //}

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("[action]")]

        public IActionResult FileEntry_Files( IFormCollection fc)
        {
            try
            {

       
            int lastFileentryID = 0;
            string CurrentMonth = DateTime.Now.ToString("MMMM");
            string CurrentYear = DateTime.Now.ToString("yyyy");
            Error obj = new Error();
            obj.message = "File Save Successfully";
            obj.error = "0";

            var productlist = _context.Admin_MProduct.Where(u => u.ProductID == Convert.ToInt32(fc["ProductID"][0])).FirstOrDefault();
            string Product = productlist.Description;

            int FolderID = 1;
            var serverDocuments = _context.ServerDocuments.Where(s => s.ID == FolderID).FirstOrDefault();
            var FolderPath = serverDocuments.FolderPath;
            var AbsolutePath = serverDocuments.AbsolutePath;
            List<FileEntry> fileentrylist = _context.PP_TFileEntry.ToList();
            if (fileentrylist.Count == 0)
            {
                lastFileentryID = 0;
            }
            else
            {
                lastFileentryID = _context.PP_TFileEntry.OrderByDescending(f => f.FileEntryID).FirstOrDefault().FileEntryID;
            }


            string varTransactionNumber = "File-" + (lastFileentryID + 1) + "-" + CurrentYear;
            List<Attachments> attachments = new List<Attachments>();
            for (int i = 0; i < fc.Files.Count; i++)        {

               
                
                   
               


                string PhysicalPath = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + varTransactionNumber;// + Process;

                string path = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + varTransactionNumber + "/" + fc["ImpressionNo"][0] + "_" + (i + 1).ToString() + "." + fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1);// + Process;
                string Virtualpath = AbsolutePath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + varTransactionNumber + "/" + fc["ImpressionNo"][0] + "_" + (i + 1).ToString() + "." + fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1);// + Process;

                if (!System.IO.Directory.Exists(PhysicalPath))
                {
                    System.IO.Directory.CreateDirectory(PhysicalPath);
                }

                if (!System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }


                //byte[] ImageData = new byte[fc.Files[i].Length];
                //System.IO.File.WriteAllBytes(path, ImageData);


                var stream = new FileStream(path, FileMode.Create);
                fc.Files[i].CopyTo(stream);
                stream.Close();

                //using (var stream1 = fc.Files[i].OpenReadStream())
                //      fc.Files[i].Upload(stream1, fileName);



                //fc.Files[i].OpenReadStream
                //byte[] buffer = await fc.Files[i].ReadAsByteArrayAsync();



                //using (System.IO.MemoryStream ms = new System.IO.MemoryStream(buffer))
                //{
                //    if (!System.IO.File.Exists(varPhysicalPath))
                //        File.WriteAllBytes(varPhysicalPath, buffer);
                //}
                attachments.Add(new Attachments
                {
                    TransactionNumber = varTransactionNumber,
                    FileName = fc["ImpressionNo"][0] + "_" + (i + 1).ToString(),
                    UploadedFileName = fc.Files[i].FileName,
                    FileType = fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1),
                    FolderID = 1,
                    PhysicalFilePath = path,
                    VirtualFilePath = Virtualpath,
                    isUploaded = false,
                    ScannedFileType = fc["Filetype"][i]

                }); ;
            }           


            FileEntry objFileEntry = new FileEntry();
            objFileEntry.TransactionNumber = varTransactionNumber;
            objFileEntry.TransactionDate = Convert.ToDateTime(fc["TransactionDate"][0]);
            objFileEntry.StatusID = 1;//open
            objFileEntry.CaseTypeID = Convert.ToInt32(fc["CaseTypeID"][0]);
            objFileEntry.ScannerTypeID = Convert.ToInt32(fc["ScannerTypeID"][0]);
            objFileEntry.ProductID = Convert.ToInt32(fc["ProductID"][0]);
            objFileEntry.CaseEntryNumber ="";
            objFileEntry.ImpressionNo = fc["ImpressionNo"][0];
            objFileEntry.TrayColor =fc["TrayColor"][0];
            objFileEntry.IsPrime = Convert.ToBoolean(fc["IsPrime"][0]);
            objFileEntry.Units = Convert.ToInt32(fc["Units"][0]);
            objFileEntry.Shade = fc["Shade"][0];
            objFileEntry.RedoImpressionNo = fc["RedoImpressionNo"][0] =="null" ? "" : fc["RedoImpressionNo"][0];
            objFileEntry.Remark = fc["Remark"][0];
            objFileEntry.LastActivityByID = Convert.ToInt32(fc["LastActivityByID"][0]);
            objFileEntry.LastActivityDate =Convert.ToDateTime(fc["LastActivityDate"][0]);

            //Section to save fileentry in db-------------


            try
            {
                _context.PP_TFileEntry.Add(objFileEntry);
                _context.SaveChanges();

            }

            catch (DbUpdateConcurrencyException ex)
            {
                obj = Models.Common.CommonException(ex);

            }
            catch (DbUpdateException ex)
            {
                obj = Models.Common.CommonException(ex);

            }

            //Section to save files on physical path-------------
            //List<Attachments> attachments = new List<Attachments>();
            //for (int i = 0; i < fc.Files.Count; i++)
            //{
            //    attachments.Add(new Attachments
            //    {

            //        FileName = objFileEntry.ImpressionNo + "_" + (i + 1).ToString(),
            //        UploadedFileName = fc.Files[i].FileName,
            //        FileType = fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1),
            //        FolderID = 1,
            //        PhysicalFilePath = serverDocuments.AbsolutePath,
            //        VirtualFilePath = serverDocuments.AbsolutePath
            //    });
            //}

            for (int i = 0; i < attachments.Count; i++)
            {

                FileEntryDetail fileEntryDetail = new FileEntryDetail();
                fileEntryDetail.FileEntryID = lastFileentryID +1;
                fileEntryDetail.LastActivityByID = objFileEntry.LastActivityByID;
                fileEntryDetail.LastActivityDate = objFileEntry.LastActivityDate;
                fileEntryDetail.ProcessID = 1;
                fileEntryDetail.FolderID = attachments[i].FolderID;
                fileEntryDetail.FileName = attachments[i].FileName;
                fileEntryDetail.UploadedFileName = attachments[i].UploadedFileName;
                fileEntryDetail.VirtualFilePath = attachments[i].VirtualFilePath;
                fileEntryDetail.PhysicalFilePath = attachments[i].PhysicalFilePath;
                fileEntryDetail.FileType = attachments[i].FileType;
                fileEntryDetail.ScannedFileType = attachments[i].ScannedFileType;
                fileEntryDetail.TransactionNumber = attachments[i].TransactionNumber;
                fileEntryDetail.isUploaded = attachments[i].isUploaded;
                _context.PP_TFileEntryDetail.Add(fileEntryDetail);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
                catch (DbUpdateException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
            }



            string Data =
                   "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                   "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));

            }
            catch (Exception ex )
            {
                Error obj = new Error();
                obj.message = ex.Message;
                obj.error = "1";

                string Data =
                     "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                     "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
                return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult FileEntry_Download(UpdateFileEntry file)
        {
            var userid = new SqlParameter("@UserID", file.UserID);
            var productid = new SqlParameter("@ProductID", file.ProductID);
            var statusid = new SqlParameter("@StatusID", file.StatusID);
            var processid = new SqlParameter("@ProcessID", file.ProcessID);
            var transactionno = new SqlParameter("@Transactionnumber", file.TransactionNumber);
            var situationid = new SqlParameter("@SituationID", file.SituationID);

                var data = _context.Database.ExecuteSqlRaw("exec USP_UPDATE_PP_TFileEntry @UserID, @ProductID,@StatusID,@ProcessID,@Transactionnumber,@SituationID",
                userid, productid, statusid, processid, transactionno, situationid);

            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);

        }
        [Route("[action]")]
        [RequestFormLimits(MultipartBodyLengthLimit =10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [HttpPost]
        public IActionResult FileEntry_Upload(IFormCollection fc)
        {
           
            string CurrentMonth = DateTime.Now.ToString("MMMM");
            string CurrentYear = DateTime.Now.ToString("yyyy");
            Error obj = new Error();
            obj.message = "File Upload Successfully";
            obj.error = "0";
            var Product = fc["Product"][0];
            var TransactionNumber = fc["TransactionNumber"][0];
            var FileEntryID = fc["FileEntryID"][0];
            var LastActivityByID= fc["LastActivityByID"][0];
            var LastActivityDate = fc["LastActivityDate"][0];
            var UserID = fc["UserID"][0];
            var ProductID = fc["ProductID"][0];
            var StatusID = fc["StatusID"][0];
            var ProcessID = fc["ProcessID"][0];
            var SituationID = fc["SituationID"][0];

            int FolderID = 1;
            var serverDocuments = _context.ServerDocuments.Where(s => s.ID == FolderID).FirstOrDefault();
            var FolderPath = serverDocuments.FolderPath;
            var AbsolutePath = serverDocuments.AbsolutePath;
            List<Attachments> attachments = new List<Attachments>();
            for (int i = 0; i < fc.Files.Count; i++)
            {

                string PhysicalPath = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + TransactionNumber;// + Process;

                string path = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + TransactionNumber + "/" + fc["TransactionNumber"][0]+ "_"+ ProcessID + "_" + (i + 1).ToString() + "." + fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1);// + Process;
               
                string Virtualpath = AbsolutePath + CurrentYear + "/" + CurrentMonth + "/" + Product + "/" + TransactionNumber + "/" + fc["TransactionNumber"][0] + "_" + ProcessID + "_" + (i + 1).ToString() + "." + fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1);// + Process;

                if (!System.IO.Directory.Exists(PhysicalPath))
                {
                    System.IO.Directory.CreateDirectory(PhysicalPath);
                }

                if (!System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                    var stream = new FileStream(path, FileMode.Create);
                fc.Files[i].CopyTo(stream);
                stream.Close();

                
                attachments.Add(new Attachments
                {

                    FileName = fc["TransactionNumber"][0] +"_" + ProcessID + "_" + (i + 1).ToString(),
                    UploadedFileName = fc.Files[i].FileName,
                    FileType = fc.Files[i].FileName.Substring(fc.Files[i].FileName.LastIndexOf('.') + 1),
                    FolderID = 1,
                    PhysicalFilePath = path,
                    VirtualFilePath = Virtualpath,
                    isUploaded = true,
                    ScannedFileType = fc["Filetype"][i],
                    TransactionNumber = TransactionNumber
                });
            }
            for (int i = 0; i < attachments.Count; i++)
            {

                FileEntryDetail fileEntryDetail = new FileEntryDetail();
                fileEntryDetail.FileEntryID = Convert.ToInt32(FileEntryID);
                fileEntryDetail.LastActivityByID = Convert.ToInt32(LastActivityByID);
                fileEntryDetail.LastActivityDate = Convert.ToDateTime(LastActivityDate);
                fileEntryDetail.ProcessID = Convert.ToInt32(0);
                fileEntryDetail.FolderID = attachments[i].FolderID;
                fileEntryDetail.FileName = attachments[i].FileName;
                fileEntryDetail.UploadedFileName = attachments[i].UploadedFileName;
                fileEntryDetail.VirtualFilePath = attachments[i].VirtualFilePath;
                fileEntryDetail.PhysicalFilePath = attachments[i].PhysicalFilePath;
                fileEntryDetail.FileType = attachments[i].FileType;
                fileEntryDetail.ScannedFileType = attachments[i].ScannedFileType;
                fileEntryDetail.TransactionNumber = attachments[i].TransactionNumber;
                fileEntryDetail.isUploaded = attachments[i].isUploaded;
                _context.PP_TFileEntryDetail.Add(fileEntryDetail);
                try
                {
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
                catch (DbUpdateException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
            }

            var userid = new SqlParameter("@UserID", UserID);
            var productid = new SqlParameter("@ProductID",ProductID);
            var statusid = new SqlParameter("@StatusID", StatusID);
            var processid = new SqlParameter("@ProcessID", ProcessID);
            var transactionno = new SqlParameter("@Transactionnumber",TransactionNumber);
            var situationid = new SqlParameter("@SituationID",SituationID);
            try
            {
                var data = _context.Database.ExecuteSqlRaw("exec USP_UPDATE_PP_TFileEntry @UserID, @ProductID,@StatusID,@ProcessID,@Transactionnumber,@SituationID",
                              userid, productid, statusid, processid, transactionno, situationid);
            }
            catch (Exception ex1)
            {
                obj.message = ex1.Message;
                obj.error = "0";
            }
          

            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + "}";
            return Ok(Data);

        }




        [Route("[action]")]
        [HttpPost]
        public IActionResult SetRejectReason(UpdateFileEntry file)
        {
            string data = "";
            var userid = new SqlParameter("@UserID", file.UserID);
            var productid = new SqlParameter("@ProductID", file.ProductID);
            var statusid = new SqlParameter("@StatusID", file.StatusID);
            var processid = new SqlParameter("@ProcessID", file.ProcessID);
            var transactionno = new SqlParameter("@Transactionnumber", file.TransactionNumber);
            var situationid = new SqlParameter("@SituationID", file.SituationID);
            var RejectReason = new SqlParameter("@RejectReason", file.RejectReason);


            int numofrowupdated = _context.Database.ExecuteSqlRaw("exec USP_UPDATE_PP_TFileEntry @UserID, @ProductID,@StatusID,@ProcessID,@Transactionnumber,@SituationID,@RejectReason",
            userid, productid, statusid, processid, transactionno, situationid, RejectReason);

            if(numofrowupdated>0)
            {
                data = "Case Rejected successfully";
            }
            else
            {
                data = "Case not Rejected successfully";
            }
            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);

        }





        // GET: api/FileEntries
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FileEntry>>> GetPP_TFileEntry()
        {
            return await _context.PP_TFileEntry.ToListAsync();
        }

        // GET: api/FileEntries/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FileEntry>> GetFileEntry(int id)
        {
            var fileEntry = await _context.PP_TFileEntry.FindAsync(id);

            if (fileEntry == null)
            {
                return NotFound();
            }

            return fileEntry;
        }

        // PUT: api/FileEntries/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFileEntry(int id, FileEntry fileEntry)
        {
            if (id != fileEntry.FileEntryID)
            {
                return BadRequest();
            }

            _context.Entry(fileEntry).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FileEntryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/FileEntries
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<FileEntry>> PostFileEntry(FileEntry fileEntry)
        {
            _context.PP_TFileEntry.Add(fileEntry);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFileEntry", new { id = fileEntry.FileEntryID }, fileEntry);
        }

        // DELETE: api/FileEntries/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FileEntry>> DeleteFileEntry(int id)
        {
            var fileEntry = await _context.PP_TFileEntry.FindAsync(id);
            if (fileEntry == null)
            {
                return NotFound();
            }

            _context.PP_TFileEntry.Remove(fileEntry);
            await _context.SaveChangesAsync();

            return fileEntry;
        }

        private bool FileEntryExists(int id)
        {
            return _context.PP_TFileEntry.Any(e => e.FileEntryID == id);
        }
    }
}
