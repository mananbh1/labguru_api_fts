﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        private readonly SqlDbContext _context;



        public CommonController(SqlDbContext context)
        {
            _context = context;

        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetCommonList(CommonInPut objCommonInPut)
        {
            if (objCommonInPut.PageName == "FileEntry")
            {
                var UserID = new SqlParameter("@UserID", objCommonInPut.UserID);
                var ProcessID = new SqlParameter("@ProcessID", objCommonInPut.UserID);
                SqlDbContext _context = new SqlDbContext();

                var SituationID3 = new SqlParameter("@SituationID", 3); //Product list User wise without process
                var Data3 = _context.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID3).ToList();
                _context.Dispose();
                SqlDbContext _context2 = new SqlDbContext();

                var SituationID = new SqlParameter("@SituationID", 1);  //CaseType
                var Data1 = _context2.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID).ToList();
                _context.Dispose();
                SqlDbContext _context3 = new SqlDbContext();

                var SituationID2 = new SqlParameter("@SituationID", 2);//ScannerType
                var Data2 = _context3.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID2).ToList();


                if (Data1.Count > 0)
                {
                    string Data =
                            "{\"Data1\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Data1) + "," +
                            "\"Data2\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Data2) + "," +
                            "\"Data3\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Data3) + "}";
                    ;
                    return Ok(Data);
                }
                else
                {
                    return NotFound();
                }
            }
            if (objCommonInPut.PageName == "Dashboard")//Process list User wise
            {
                var UserID = new SqlParameter("@UserID", objCommonInPut.UserID);
                var ProcessID = new SqlParameter("@ProcessID", 0);
                var SituationID = new SqlParameter("@SituationID", 5);
                if (ProcessID.Value == null)
                {
                    ProcessID.Value = 0;
                }
                var Processlist = _context.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID).ToList();
                if (Processlist.Count > 0)
                {
                    string Data =
                            "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Processlist) + "}";
                    return Ok(Data);
                }
                else
                {
                    return NotFound();
                }
            }
            if (objCommonInPut.PageName == "SearchFile")
            {
                var UserID = new SqlParameter("@UserID", objCommonInPut.UserID);
                var ProcessID = new SqlParameter("@ProcessID", 0);
                var SituationID1 = new SqlParameter("@SituationID", 6);//StatusList
                if (ProcessID.Value == null)
                {
                    ProcessID.Value = 0;
                }
                var Statuslist = _context.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID1).ToList();
                _context.Dispose();

                SqlDbContext _context2 = new SqlDbContext();
                var SituationID3 = new SqlParameter("@SituationID", 5); //Process list User wise 
                var ProcessList = _context2.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID3).ToList();
                _context2.Dispose();


                string Data =
                           "{\"Statuslist\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Statuslist) + "," +
                        "\"ProcessList\":" + Newtonsoft.Json.JsonConvert.SerializeObject(ProcessList) + "}";
                ;
                return Ok(Data);

            }
            if (objCommonInPut.PageName == "ProcessMaster")
            {
                var UserID = new SqlParameter("@UserID", objCommonInPut.UserID);
                var ProcessID = new SqlParameter("@ProcessID", 0);
                var SituationID = new SqlParameter("@SituationID", 8);//Acton List
                if (ProcessID.Value == null)
                {
                    ProcessID.Value = 0;
                }
                var ActionList = _context.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID).ToList();
                string Data =
                           "{\"ActionList\":" + Newtonsoft.Json.JsonConvert.SerializeObject(ActionList) + "}";

                return Ok(Data);

            }
            if (objCommonInPut.PageName == "MillingSearch")
            {
                var UserID = new SqlParameter("@UserID", objCommonInPut.UserID);
                var ProcessID = new SqlParameter("@ProcessID", 0);
                var SituationID = new SqlParameter("@SituationID", 10);//status List For Milling search file
                if (ProcessID.Value == null)
                {
                    ProcessID.Value = 0;
                }
                var StatusList = _context.Common.FromSqlRaw<CommonOutPut>("exec usp_Select_Common_DLL @UserID,@ProcessID,@SituationID", UserID, ProcessID, SituationID).ToList();
                string Data =
                           "{\"StatusList\":" + Newtonsoft.Json.JsonConvert.SerializeObject(StatusList) + "}";

                return Ok(Data);

            }
            return Ok();
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetCommonValidation(CommonValidationInput objCommonValidationInPut)
        {
            
                var SituationID = new SqlParameter("@SituationID", objCommonValidationInPut.SituationID);  //Impression No validation
                var UserID = new SqlParameter("@UserID", objCommonValidationInPut.UserID);
                var ImpressionNo = new SqlParameter("@ImpressionNo", objCommonValidationInPut.ImpressionNo);
                var TransactionNo = new SqlParameter("@TransactionNo", objCommonValidationInPut.TransactionNo);
                var StatusID = new SqlParameter("@StatusID", objCommonValidationInPut.StatusID);
                var ProductID = new SqlParameter("@ProductID", objCommonValidationInPut.ProductID);
                SqlDbContext _context = new SqlDbContext();

                if (TransactionNo.Value == null)
                {
                    TransactionNo.Value = "";
                }
                
                if (ImpressionNo.Value == null)
                {
                    ImpressionNo.Value = 0;
                }
                if (ProductID.Value == null)
                {
                    ProductID.Value = 0;
                }

            var Data1 = _context.CommonValidationOutput.FromSqlRaw("exec usp_Select_Common_Validation @SituationID ,@UserID,@ImpressionNo,@TransactionNo,@StatusID,@ProductID", SituationID, UserID, ImpressionNo, TransactionNo, StatusID,ProductID).ToList();
                _context.Dispose();
                string Data ="{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Data1)  + "}";;
                return Ok(Data);
        }
    }
}
