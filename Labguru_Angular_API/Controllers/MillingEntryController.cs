﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using System.IO;
using Labguru_Angular_API.Models;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MillingEntryController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public MillingEntryController(SqlDbContext context)
        {
            _context = context;
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult GetImpressionDetails(MillingEntryInputDTO objmillinginput)
        {


            var productgroupid = new SqlParameter("@ProductGroupID", objmillinginput.ProductGroupID);
            var impressionno = new SqlParameter("@ImpressionNo", objmillinginput.ImpressionNo);

            var tractionnumber = new SqlParameter("@TransactionNunber", objmillinginput.TransactionNumber);
            var situationid = new SqlParameter("@SituationID", 0);
            if (situationid.Value == null)
            {
                situationid.Value = 0;
            }
            var userid = new SqlParameter("@UserID", 0);
            if (userid.Value == null)
            {
                userid.Value = 0;
            }
            var statusid = new SqlParameter("@StatusID", objmillinginput.StatusID);
            if (statusid.Value == null)
            {
                statusid.Value = 0;
            }
            var ImpressionDetailsList = _context.MillingEntryImpressionNo.FromSqlRaw("exec USP_SELECT_PP_TMillingEntry @ProductGroupID,@ImpressionNo,@TransactionNunber,@SituationID,@UserID,@StatusID", productgroupid, impressionno, tractionnumber, situationid, userid, statusid).ToList();
            string Data =
                       "{\"ImpressionDetailsList\":" + Newtonsoft.Json.JsonConvert.SerializeObject(ImpressionDetailsList) + "}";

            return Ok(Data);
        }


        [Route("[action]")]
        [HttpPost]
        public IActionResult GetMillingDashboard(MillingDashboardInputDTO millingdashboard)
        {


            List<CommonOutPut> objProductGroup = new List<CommonOutPut>();
            var userid = new SqlParameter("@UserID", millingdashboard.UserID);
            var processid = new SqlParameter("@ProcessID", millingdashboard.ProcessID);
            if (processid.Value == null)
            {
                processid.Value = 0;
            }
            var situationid = new SqlParameter("@SituationID", 9);
            objProductGroup = _context.Common.FromSqlRaw("exec usp_Select_Common_DLL @UserID, @ProcessID,@SituationID", userid, processid, situationid).ToList();
            List<MillingDashboard> MillingDashboard = new List<MillingDashboard>();
            for (int x = 0; x < objProductGroup.Count; x++)
            {
                List<MillingDashboardOutputDTO> objMillingDashboardOutputDTO = new List<MillingDashboardOutputDTO>();
                List<MillingDashboardOutputDTO> obj = new List<MillingDashboardOutputDTO>();

                var param1 = new SqlParameter("@ProductGroupID", objProductGroup[x].ID);
                var param2 = new SqlParameter("@ImpressionNo", "");
                var param3 = new SqlParameter("@TransactionNunber", "");
                var param4 = new SqlParameter("@SituationID", 1);
                var param5 = new SqlParameter("@UserID", millingdashboard.UserID);
                var param6 = new SqlParameter("@StatusID", 0);

                if (param6.Value == null)
                {
                    param6.Value = 0;
                }
                _context.Database.SetCommandTimeout(180);
                objMillingDashboardOutputDTO = _context.MillingDashboardOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TMillingEntry @ProductGroupID,@ImpressionNo,@TransactionNunber,@SituationID,@UserID,@StatusID", param1, param2, param3, param4, param5, param6).ToList();

                MillingDashboard.Add(new MillingDashboard { MillingDashboardOutputDTO = objMillingDashboardOutputDTO, ProductGroupID = objProductGroup[x].ID, ProductGroup = objProductGroup[x].Description });
            }

            if (MillingDashboard.Count > 0)
            {
                string Data =
                        "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(MillingDashboard) + "}";
                return Ok(Data);
            }
            else
            {
                return NotFound();
            }


        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetMillingEntryFiles(MillingEntryInputDTO objmillinginput)
        {


            var productgroupid = new SqlParameter("@ProductGroupID", objmillinginput.ProductGroupID);
            if (productgroupid.Value == null)
            {
                productgroupid.Value = 0;
            }
            var impressionno = new SqlParameter("@ImpressionNo", objmillinginput.ImpressionNo);
            if (impressionno.Value == null)
            {
                impressionno.Value = "";
            }
            var tractionnumber = new SqlParameter("@TransactionNunber", objmillinginput.TransactionNumber);
            if (tractionnumber.Value == null)
            {
                tractionnumber.Value = "";
            }
            var situationid = new SqlParameter("@SituationID", 2);
            if (situationid.Value == null)
            {
                situationid.Value = 0;
            }
            var userid = new SqlParameter("@UserID", objmillinginput.UserID);
            if (userid.Value == null)
            {
                userid.Value = 0;
            }
            var statusid = new SqlParameter("@StatusID", objmillinginput.StatusID);
            if (statusid.Value == null)
            {
                statusid.Value = 0;
            }
            var MillingSearchFileDetails = _context.MillingEntrySearchOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TMillingEntry @ProductGroupID,@ImpressionNo,@TransactionNunber,@SituationID,@UserID,@StatusID", productgroupid, impressionno, tractionnumber, situationid, userid, statusid).ToList();
            string Data =
                       "{\"MillingSearchFileDetails\":" + Newtonsoft.Json.JsonConvert.SerializeObject(MillingSearchFileDetails) + "}";

            return Ok(Data);
        }
        [Route("[action]")]
        [HttpPost]
        public IActionResult GetImpressionDetailsOnlink(MillingEntryInputDTO objmillinginput)
        {


            var productgroupid = new SqlParameter("@ProductGroupID", objmillinginput.ProductGroupID);
            if (productgroupid.Value == null)
            {
                productgroupid.Value = 0;
            }
            var impressionno = new SqlParameter("@ImpressionNo", objmillinginput.ImpressionNo);
            if (impressionno.Value == null)
            {
                impressionno.Value = " ";
            }
            var tractionnumber = new SqlParameter("@TransactionNunber", objmillinginput.TransactionNumber);
            if (tractionnumber.Value == null)
            {
                tractionnumber.Value = "";
            }
            var situationid = new SqlParameter("@SituationID", 3);
            if (situationid.Value == null)
            {
                situationid.Value = 0;
            }
            var userid = new SqlParameter("@UserID", objmillinginput.UserID);
            if (userid.Value == null)
            {
                userid.Value = 0;
            }
            var statusid = new SqlParameter("@StatusID", objmillinginput.StatusID);
            if (statusid.Value == null)
            {
                statusid.Value = 0;
            }
            var MillingEntryImpressionNo = _context.MillingEntryImpressionOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TMillingEntry @ProductGroupID,@ImpressionNo,@TransactionNunber,@SituationID,@UserID,@StatusID", productgroupid, impressionno, tractionnumber, situationid, userid, statusid).ToList();
            string Data =
                       "{\"MillingEntryImpressionNo\":" + Newtonsoft.Json.JsonConvert.SerializeObject(MillingEntryImpressionNo) + "}";

            return Ok(Data);
        }

        [HttpPost]
        [RequestFormLimits(MultipartBodyLengthLimit = 10000000000)]
        [DisableRequestSizeLimit]
        [Consumes("multipart/form-data")]
        [Route("[action]")]
        public IActionResult FileEntry_Milling(IFormCollection fc)
        {
            try
            {


                int lastMillingEntryID = 0;
                string CurrentMonth = DateTime.Now.ToString("MMMM");
                string CurrentYear = DateTime.Now.ToString("yyyy");
                Error obj = new Error();
                obj.message = "Milling File Save Successfully";
                obj.error = "0";

                var productgrouplist = _context.Admin_MProductGroupMaster.Where(u => u.ProductGroupID == Convert.ToInt32(fc["ProductGroupID"][0])).FirstOrDefault();
                string ProductGroup = productgrouplist.Description;

                int FolderID = 1;
                var serverDocuments = _context.ServerDocuments.Where(s => s.ID == FolderID).FirstOrDefault();
                var FolderPath = serverDocuments.FolderPath;
                var AbsolutePath = serverDocuments.AbsolutePath;


                List<MillingEntry> millingEntries = _context.PP_TMillingEntry.ToList();
                if (millingEntries.Count == 0)
                {
                    lastMillingEntryID = 0;
                }
                else
                {
                    lastMillingEntryID = _context.PP_TMillingEntry.OrderByDescending(f => f.MillingEntryID).FirstOrDefault().MillingEntryID;
                }


                string varTransactionNumber = "Milling-" + (lastMillingEntryID + 1) + "-" + CurrentYear;

                //Milling File
                string PhysicalPath = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + ProductGroup + "/" + varTransactionNumber;// + Process;
                string path = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + ProductGroup + "/" + varTransactionNumber + "_Milling." + fc.Files[0].FileName.Substring(fc.Files[0].FileName.LastIndexOf('.') + 1);
                string Millingfileurl = AbsolutePath + CurrentYear + "/" + CurrentMonth + "/" + ProductGroup + "/" + varTransactionNumber + "_Milling." + fc.Files[0].FileName.Substring(fc.Files[0].FileName.LastIndexOf('.') + 1); ;
                if (!System.IO.Directory.Exists(PhysicalPath))
                {
                    System.IO.Directory.CreateDirectory(PhysicalPath);
                }

                if (!System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                var stream1 = new FileStream(path, FileMode.Create);
                fc.Files[0].CopyTo(stream1);
                stream1.Close();

                //PrintOutFile
                string path1 = FolderPath + CurrentYear + "/" + CurrentMonth + "/" + ProductGroup + "/" + varTransactionNumber + "_Printout." + fc.Files[1].FileName.Substring(fc.Files[1].FileName.LastIndexOf('.') + 1);
                string PrintOutfileurl = AbsolutePath + CurrentYear + "/" + CurrentMonth + "/" + ProductGroup + "/" + varTransactionNumber + "_Printout." + fc.Files[1].FileName.Substring(fc.Files[1].FileName.LastIndexOf('.') + 1); ;
                if (!System.IO.Directory.Exists(PhysicalPath))
                {
                    System.IO.Directory.CreateDirectory(PhysicalPath);
                }

                if (!System.IO.File.Exists(path))
                {
                    System.IO.File.Delete(path);
                }
                var stream2 = new FileStream(path1, FileMode.Create);
                fc.Files[1].CopyTo(stream2);

                stream2.Close();


                MillingEntry objMillingEntry = new MillingEntry();
                objMillingEntry.TransactionNumber = varTransactionNumber;
                objMillingEntry.TransactionDate = Convert.ToDateTime(fc["TransactionDate"][0]);
                objMillingEntry.StatusID = 1;//open
                objMillingEntry.ProductGroupID = Convert.ToInt32(fc["ProductGroupID"][0]);
                objMillingEntry.LotNo = Convert.ToString(fc["LotNo"][0]);
                objMillingEntry.BarcodeNo = Convert.ToString(fc["BarcodeNo"][0]);
                objMillingEntry.MillingFile_URL = Millingfileurl;
                objMillingEntry.PrintOutFile_URL = PrintOutfileurl;
                objMillingEntry.Remark = Convert.ToString(fc["Remark"][0]); ;
                objMillingEntry.LastActivityByID = Convert.ToInt32(fc["LastActivityByID"][0]);
                objMillingEntry.LastActivityDate = Convert.ToDateTime(fc["LastActivityDate"][0]);

                //Section to save fileentry in db-------------


                try
                {
                    _context.PP_TMillingEntry.Add(objMillingEntry);
                    _context.SaveChanges();

                }

                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
                catch (DbUpdateException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }

                string str = fc["ImpressionNo[]"];
                List<string> str1 = str.Split(',').ToList();

                if (str1.Count > 0)
                {
                    for (int i = 0; i < str1.Count; i++)
                    {



                        MillingEntryDetail millingfileEntryDetail = new MillingEntryDetail();
                        millingfileEntryDetail.MillingEntryID = lastMillingEntryID + 1;
                        millingfileEntryDetail.LastActivityByID = objMillingEntry.LastActivityByID;
                        millingfileEntryDetail.LastActivityDate = objMillingEntry.LastActivityDate;
                        millingfileEntryDetail.TransactionNumber = varTransactionNumber;
                        millingfileEntryDetail.ImpressionNo = str1[i];
                        _context.PP_TMillingEntryDetail.Add(millingfileEntryDetail);

                        try
                        {
                            _context.SaveChanges();
                        }
                        catch (DbUpdateConcurrencyException ex)
                        {
                            obj = Models.Common.CommonException(ex);

                        }
                        catch (DbUpdateException ex)
                        {
                            obj = Models.Common.CommonException(ex);

                        }
                    }
                }

                string Data =
                       "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                       "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
                return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));

            }
            catch (Exception ex)
            {
                Error obj = new Error();
                obj.message = ex.Message;
                obj.error = "1";

                string Data =
                     "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                     "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
                return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
            }
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult MillingFileEntry_Download(UpdateMillingFileEntryInputDTO file)
        {
            var userid = new SqlParameter("@UserID", file.UserID);
            var statusid = new SqlParameter("@StatusID", file.StatusID);
            var transactionno = new SqlParameter("@Transactionnumber", file.TransactionNumber);
            

            var data = _context.Database.ExecuteSqlRaw("exec USP_UPDATE_PP_TMillingFileEntry @UserID, @StatusID,@Transactionnumber",
            userid, statusid, transactionno);

            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);

        }
    }
}
