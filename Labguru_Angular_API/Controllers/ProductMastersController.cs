﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.AspNetCore.Authorization;
using Labguru_Angular_API.Models;
using Microsoft.Data.SqlClient;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductMastersController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public ProductMastersController(SqlDbContext context)
        {
            _context = context;
        }



        // GET: api/ProductMasters
        [HttpGet]
        public List<SelectProductMaster> GetAdmin_MProcess(int SituationID)
        {
            var param1 = new SqlParameter("@SituationID", SituationID);
            return _context.SelectProductMaster.FromSqlRaw("exec usp_select_Admin_MProduct @SituationID ", param1).ToList();

        }

        // GET: api/ProductMasters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductMaster>> GetProductMaster(int id)
        {
            var productMaster = await _context.Admin_MProduct.FindAsync(id);

            if (productMaster == null)
            {
                return NotFound();
            }

            return productMaster;
        }

        // PUT: api/ProductMasters/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductMaster(int id, ProductMaster productMaster)
        {
            Error obj = new Error();

            obj.message = "Product Updated Successfully";
            obj.error = "0";
          

            if (id != productMaster.ProductID)
            {
                return BadRequest();
            }

            try
            {
                _context.Update(productMaster);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (!ProductMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    obj = Models.Common.CommonException(ex);
                }
            }

            string Data =
                    "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                    "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
            //_context.Entry(productMaster).State = EntityState.Modified;
            //_context.Entry(productMaster).Property(x => x.Description).IsModified = false; //Add fields which you don't want to modify
            //_context.SaveChanges();
            //try
            //{
            //    //await _context.SaveChangesAsync();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!ProductMasterExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            //return NoContent();
        }

        // POST: api/ProductMasters
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ProductMaster>> PostProductMaster(ProductMaster productMaster)
        {
            //_context.Admin_MProduct.Add(productMaster);
            //await _context.SaveChangesAsync();

            //return CreatedAtAction("GetProductMaster", new { id = productMaster.ProductID }, productMaster);

            Error obj = new Error();

            obj.message = "Product Added Successfully";
            obj.error = "0";
           
            _context.Admin_MProduct.Add(productMaster);
            try
            {

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException ex)
            {
                obj = Models.Common.CommonException(ex);

            }

            string Data =
                     "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                     "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));

        }

        // DELETE: api/ProductMasters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductMaster>> DeleteProductMaster(int id)
        {
            var productMaster = await _context.Admin_MProduct.FindAsync(id);
            if (productMaster == null)
            {
                return NotFound();
            }

            _context.Admin_MProduct.Remove(productMaster);
            await _context.SaveChangesAsync();

            return productMaster;
        }

        private bool ProductMasterExists(int id)
        {
            return _context.Admin_MProduct.Any(e => e.ProductID == id);
        }
    }
}
