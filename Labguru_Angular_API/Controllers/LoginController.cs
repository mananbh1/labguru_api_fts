﻿using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        public SqlDbContext _sqlcontext = new SqlDbContext();
        private Microsoft.Extensions.Configuration.IConfiguration _config;

        public LoginController(IConfiguration config)
        {
            _config = config;
        }
        //GET: api/Login
        //[HttpGet]
        // public async Task<ActionResult<IEnumerable<Users>>> GetAdmin_Muser()
        // {
        //     //return await _sqlcontext.Admin_Muser.ToListAsync();
        //     return await _sqlcontext.Admin_Muser.FromSqlRaw("exec usp_select_Admin_MProcess").ToList();

        // }
        [Route("[action]")]
        [HttpGet]
        public IActionResult GetAdmin_Muser()
        {
            var data = _sqlcontext.Admin_Muser.FromSqlRaw("exec usp_select_Admin_MUser").ToList();
            return Ok(data);

        }



        // GET: api/Login/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Login
        [Route("[action]")]
        [HttpPost]
        public IActionResult PostLogin(Login_Input UserDTO)
        {
            var param1 = new SqlParameter("@Code", UserDTO.Code);
            var param2 = new SqlParameter("@Password", UserDTO.Password);
            var param3 = new SqlParameter("@SituationID", "0");
            var data = _sqlcontext.Login_Output.FromSqlRaw("exec usp_Authenticate_Login @Code,@Password, @SituationID", param1, param2, param3).ToList();

            var param4 = new SqlParameter("@SituationID", "1");
            var Menu = _sqlcontext.Menu.FromSqlRaw("exec usp_Authenticate_Login @Code,@Password, @SituationID", param1, param2, param4).ToList();


            var distncparent = Menu.Where(i => i.ParentMenuID == 0).Distinct().ToList();

            List<UserMenu> FinaluserMenu = new List<UserMenu>();
            UserMenu userMenu = new UserMenu();

            for (int i = 0; i < distncparent.Count; i++)
            {

                List<Menu> menu = new List<Menu>();
                List<Menu> chilMenu = new List<Menu>();
                chilMenu = Menu.Where(z => z.ParentMenuID == distncparent[i].MenuID).ToList();
                for (int j = 0; j < chilMenu.Count; j++)
                {
                    menu.Add(new Menu
                    {
                        MenuID = chilMenu[j].MenuID,
                        MenuName = chilMenu[j].MenuName,
                        Collapse = chilMenu[j].Collapse,
                        Icon = chilMenu[j].Icon,
                        ParentMenuID = distncparent[i].ParentMenuID,
                        Route = chilMenu[j].Route
                    });
                }

                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.MenuName = distncparent[i].MenuName;
                userMenu.Icon = distncparent[i].Icon;
                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.Route = distncparent[i].Route;
                userMenu.ChildMenu = menu;

                FinaluserMenu.Add(new UserMenu
                {
                    Collapse = distncparent[i].Collapse,
                    MenuName = distncparent[i].MenuName,
                    Icon = distncparent[i].Icon,
                    Route = distncparent[i].Route,
                    ChildMenu = menu
                });

            }
            if (data.Count > 0)
            {
                var Token = GenerateJSONWebToken(UserDTO);
                string Data =
                        "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + ", " +
                        "\"Menu\":" + Newtonsoft.Json.JsonConvert.SerializeObject(FinaluserMenu) + ", " +
                        "\"token\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Token) + "}";

                return Ok(Data);
            }
            else
            {
                return NotFound();
            }
        }
        private string GenerateJSONWebToken(Login_Input loginInfo)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new[] {
             new Claim(JwtRegisteredClaimNames.Sub, loginInfo.Code),
             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
             };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
            _config["Jwt:Issuer"],
            claims,
            expires: DateTime.Now.AddMinutes(120),
            signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        // PUT: api/Login/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }


    }
}
