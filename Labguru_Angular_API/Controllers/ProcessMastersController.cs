﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Labguru_Angular_API.Models;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessMastersController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public ProcessMastersController(SqlDbContext context)
        {
            _context = context;
        }
        


        // GET: api/ProcessMasters
        [HttpGet]
        public List<SelectProcessMaster> GetAdmin_MProcess()
        {
           
            //return await _context.Admin_MProcess.ToListAsync();
            return _context.SelectProcessMaster.FromSqlRaw("exec usp_select_Admin_MProcess").ToList();
            // return result;
        }

        // GET: api/ProcessMasters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProcessMaster>> GetProcessMaster(int id)
        {
            var processMaster = await _context.Admin_MProcess.FindAsync(id);

            if (processMaster == null)
            {
                return NotFound();
            }

            return processMaster;
        }

        // PUT: api/ProcessMasters/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProcessMaster(int id, ProcessMaster processMaster)
        {
            Error obj = new Error();

            obj.message = "Process Updated Successfully";
            obj.error = "0";

            if (id != processMaster.ProcessID)
            {
                return BadRequest();
            }

            //_context.Entry(processMaster).State = EntityState.Modified;

            try
            {
                _context.Update(processMaster);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (!ProcessMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    obj = Models.Common.CommonException(ex);
                }
            }

            string Data =
                    "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                    "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));



        }

        // POST: api/ProcessMasters
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ProcessMaster>> PostProcessMaster(ProcessMaster processMaster)
        {
            Error obj = new Error();

            obj.message = "Process Added Successfully";
            obj.error = "0";
            
            _context.Admin_MProcess.Add(processMaster);
            try
            {

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException ex)
            {
                obj = Models.Common.CommonException(ex);

            }

            string Data =
                    "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                    "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));

        }



        // DELETE: api/ProcessMasters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProcessMaster>> DeleteProcessMaster(int id)
        {
            var processMaster = await _context.Admin_MProcess.FindAsync(id);
            if (processMaster == null)
            {
                return NotFound();
            }

            _context.Admin_MProcess.Remove(processMaster);
            await _context.SaveChangesAsync();

            return processMaster;
        }

        private bool ProcessMasterExists(int id)
        {
            return _context.Admin_MProcess.Any(e => e.ProcessID == id);
        }
    }
}
