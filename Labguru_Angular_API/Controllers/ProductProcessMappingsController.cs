﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using System.Collections;
using Labguru_Angular_API.Models;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductProcessMappingsController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public ProductProcessMappingsController(SqlDbContext context)
        {
            _context = context;
        }

        // GET: api/ProcessProductMappings
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductProcessMapping>>> GetAdmin_MProductProcessMapping()
        {

            return await _context.Admin_MProductProcessMapping.ToListAsync();
        }


        [Route("[action]")]
        [HttpGet]
        public IActionResult GetProducts()
        {
            var result = from ppm in _context.Admin_MProductProcessMapping
                         join p in _context.Admin_MProduct
                         on ppm.ProductID equals p.ProductID
                         where ppm.ProductID == 1
                         select new
                         {
                             p.ProductID,
                             p.Code
                         };
            return Ok(result);
        }



        // GET: api/ProcessProductMappings/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductProcessMapping>> GetProcessProductMapping(int id)
        {
            var processProductMapping = await _context.Admin_MProductProcessMapping.FindAsync(id);

            if (processProductMapping == null)
            {
                return NotFound();
            }

            return processProductMapping;
        }

        // PUT: api/ProcessProductMappings/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProcessProductMapping(int id, ProductProcessMapping processProductMapping)
        {
            if (id != processProductMapping.PPMapID)
            {
                return BadRequest();
            }

            _context.Entry(processProductMapping).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProcessProductMappingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProcessProductMappings
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ProductProcessMapping>> PostProcessProductMapping(ProductProcessMapping processProductMapping)
        {

            _context.Admin_MProductProcessMapping.Add(processProductMapping);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProcessProductMapping", new { id = processProductMapping.PPMapID }, processProductMapping);
        }

        // DELETE: api/ProcessProductMappings/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductProcessMapping>> DeleteProcessProductMapping(int id)
        {
            var processProductMapping = await _context.Admin_MProductProcessMapping.FindAsync(id);
            if (processProductMapping == null)
            {
                return NotFound();
            }

            _context.Admin_MProductProcessMapping.Remove(processProductMapping);
            await _context.SaveChangesAsync();

            return processProductMapping;
        }

        private bool ProcessProductMappingExists(int id)
        {
            return _context.Admin_MProductProcessMapping.Any(e => e.PPMapID == id);
        }


        [Route("[action]")]
        [HttpPost]
        public List<ProductProcessMappingSelect> GetAdmin_MProductProcessMapping(ProductProcessMappingViewModel model)
        {
            var param1 = new SqlParameter("@ProductID", model.ProductID);
            return _context.SelectAdmin_MProcessProductMapping.FromSqlRaw("exec usp_select_Admin_MProductProcessMapping @ProductID", param1).ToList();

        }




        [Route("[action]")]
        [HttpPost]
        public IActionResult Save(addProductProcessMapping mappings)
        {
            Error obj = new Error();

            obj.message = "Product Process Mapped Successfully";
            obj.error = "0";

            string strProcess = mappings.Processes;
            List <string> str1 = strProcess.Split(',').ToList();

            string strSequence = mappings.Sequences;
            string strActions = mappings.Actions;
            List<string> str2 = strSequence.Split(',').ToList();
            List<string> str3 = strActions.Split(',').ToList();
            addProductProcessMapping items = new addProductProcessMapping();

            var param = new SqlParameter("@ProductID", mappings.ProductID);
            _context.Database.ExecuteSqlRaw("exec Usp_Delete_Admin_MProductProcessMapping @ProductID", param);
            _context.SaveChanges();

            for (int i = 0; i < str1.Count; i++)
            {
                items.ProductID = mappings.ProductID;
                items.ProcessID = Convert.ToInt32(str1[i]);
                items.LastActivityByID = mappings.LastActivityByID;
                items.LastActivityDate = mappings.LastActivityDate;
                items.Sequence = Convert.ToInt32(str2[i]);
                items.ActionID = Convert.ToInt32(str3[i]);
                var param1 = new SqlParameter("@ProductID", items.ProductID);
                var param2 = new SqlParameter("@ProcessID", items.ProcessID);
                var param3 = new SqlParameter("@Sequence", items.Sequence);
                var param4 = new SqlParameter("@LastActivityByID", items.LastActivityByID);
                var param5 = new SqlParameter("@LastActivityDate", items.LastActivityDate);
                var param6 = new SqlParameter("@ActionID", items.ActionID);

                try
                {
                  _context.Database.ExecuteSqlRaw("exec Usp_Insert_Admin_MProductProcessMapping  @ProductID,@ProcessID, @Sequence, @LastActivityByID, @LastActivityDate,@ActionID", param1, param2, param3, param4, param5, param6);
                  _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
            }


            string Data =
                   "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                   "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
        }
    }
 }
