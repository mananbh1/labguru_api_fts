﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Labguru_Angular_API.Models;
namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersMasterController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public UsersMasterController(SqlDbContext context)
        {
            _context = context;
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult GetUserMaster()
        {
            List<Process> objProcess = new List<Process>();
            List<Product> objProduct = new List<Product>();

            var param1 = new SqlParameter("@UserID", "0");
            var param2 = new SqlParameter("@ProcessID", "0");
            var param3 = new SqlParameter("@SituationID", "0");
            objProcess = _context.Process.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping @UserID,@ProcessID,@SituationID", param1, param2, param3).ToList();

            var param4 = new SqlParameter("@SituationID", "1");
            objProduct = _context.Product.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping @UserID,@ProcessID,@SituationID", param1, param2, param4).ToList();

            var param5 = new SqlParameter("@SituationID", "2");
            var Page = _context.AllMenuShow.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping_Changes @UserID,@ProcessID,@SituationID", param1, param2, param5).ToList();

            //  var distncProcess = objProcessProduct.Select(i => i.Process).Distinct().ToList();
            List<ProcessProduct> ProcessProduct = new List<ProcessProduct>();
            for (int x = 0; x < objProcess.Count; x++)
            {
                List<Product> obj = new List<Product>();
                foreach (var item1 in objProduct)
                {
                    obj.Add(new Product { ProductName = item1.ProductName, ProductID = item1.ProductID });
                }
                ProcessProduct.Add(new ProcessProduct { Product = obj, ProcessName = objProcess[x].ProcessName, ProcessID = objProcess[x].ProcessID });
            }

            var distncparent = Page.Where(i => i.ParentMenuID == 0).Distinct().ToList();
            List<AllUserMenu> FinaluserMenu = new List<AllUserMenu>();
            AllUserMenu userMenu = new AllUserMenu();

            for (int i = 0; i < distncparent.Count; i++)
            {

                List<AllMenuShow> menu = new List<AllMenuShow>();
                List<AllMenuShow> chilMenu = new List<AllMenuShow>();
                chilMenu = Page.Where(z => z.ParentMenuID == distncparent[i].MenuID).ToList();
                for (int j = 0; j < chilMenu.Count; j++)
                {
                    menu.Add(new AllMenuShow
                    {
                        MenuID = chilMenu[j].MenuID,
                        MenuName = chilMenu[j].MenuName,
                        Collapse = chilMenu[j].Collapse,
                        Icon = chilMenu[j].Icon,
                        ParentMenuID = distncparent[i].ParentMenuID,
                        Route = chilMenu[j].Route,
                        PageID = chilMenu[j].PageID,
                        Mapped = chilMenu[j].Mapped
                    });
                }

                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.MenuName = distncparent[i].MenuName;
                userMenu.Icon = distncparent[i].Icon;
                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.Route = distncparent[i].Route;
                userMenu.ChildMenu = menu;

                FinaluserMenu.Add(new AllUserMenu
                {
                    Collapse = distncparent[i].Collapse,
                    MenuName = distncparent[i].MenuName,
                    Icon = distncparent[i].Icon,
                    Route = distncparent[i].Route,
                    ChildMenu = menu
                });

            }


            if (ProcessProduct.Count > 0)
            {
                string Data =
                        "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(ProcessProduct) + "," +
                          "\"Page\":" + Newtonsoft.Json.JsonConvert.SerializeObject(FinaluserMenu) + "}";
                return Ok(Data);
            }
            return Ok();



        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult SaveUser(UsersMaster Userviewmodel)
        {
            Error obj = new Error();

            UserMaster_Input User = new UserMaster_Input();
            User.Code = Userviewmodel.Code;
            User.Description = Userviewmodel.Description;
            User.Password = Userviewmodel.Password;
            User.IsActive = true;
            User.FileDownloadAccess = true;
            User.LastActivityByID = Userviewmodel.LastActivityByID;
            User.LastActivityDate = Userviewmodel.LastActivityDate;
            User.UserID = Userviewmodel.UserID;


            if (Userviewmodel.UserID != 0)
            {
                obj.message = "User Update Successfully";
                obj.error = "0";
                var param = new SqlParameter("@UserID", Userviewmodel.UserID);
                _context.Database.ExecuteSqlRaw("exec Usp_Delete_Admin_MUserProcessProductMapping @UserID", param);
                try
                {
                    _context.Admin_Muser.Update(User);
                    _context.SaveChanges();

                }
                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
                catch (DbUpdateException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
            }
            else
            {
                obj.message = "User Save Successfully";
                obj.error = "0";
                try
                {
                    _context.Admin_Muser.Add(User);
                    _context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }
                catch (DbUpdateException ex)
                {
                    obj = Models.Common.CommonException(ex);

                }


            }

            int lastuserid = User.UserID;
            string str = Userviewmodel.UserProcessProduct;
            List<string> str1 = str.Split(',').ToList();

            if (str1.Count > 0)
            {
                for (int i = 0; i < str1.Count; i++)
                {

                    UserProductProcessMapping UserPPMApping = new UserProductProcessMapping();
                    UserPPMApping.LastActivityByID = Userviewmodel.LastActivityByID;
                    UserPPMApping.UserID = lastuserid;
                    UserPPMApping.LastActivityDate = Userviewmodel.LastActivityDate;
                    string[] arr = str1[i].Split('-');
                    UserPPMApping.ProcessID = arr[0];
                    UserPPMApping.ProductID = arr[1];
                    _context.Admin_MUserProcessProductMapping.Add(UserPPMApping);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        obj = Models.Common.CommonException(ex);

                    }
                    catch (DbUpdateException ex)
                    {
                        obj = Models.Common.CommonException(ex);

                    }
                }
            }

            string strPages = Userviewmodel.Pages;
            List<string> arrPages = strPages.Split(',').ToList();

            if (arrPages.Count > 0  )
            {
                for (int i = 0; i < arrPages.Count; i++)
                {

                    Admin_MUserPage Pages = new Admin_MUserPage();
                    Pages.UserID = lastuserid;
                    Pages.PageID = Convert.ToInt32(arrPages[i]);
                    _context.Admin_MUserPage.Add(Pages);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        obj = Models.Common.CommonException(ex);

                    }
                    catch (DbUpdateException ex)
                    {
                        obj = Models.Common.CommonException(ex);

                    }
                }
            }

            string Data =
                   "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                   "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult EditUserMaster(GetUserDetail users)
        {
            //Users users
            List<Process> objProcess = new List<Process>();
            var param1 = new SqlParameter("@UserID", users.UserID);
            var param2 = new SqlParameter("@ProcessID", "0");
            var param3 = new SqlParameter("@SituationID", "0");
            objProcess = _context.Process.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping @UserID,@ProcessID,@SituationID", param1, param2, param3).ToList();

            List<ProcessProduct> ProcessProduct = new List<ProcessProduct>();
            var param4 = new SqlParameter("@SituationID", "1");
            for (int x = 0; x < objProcess.Count; x++)
            {
                List<ProductMasterData> objProduct = new List<ProductMasterData>();
                List<Product> obj = new List<Product>();


                var Parm8 = new SqlParameter("@UserID", users.UserID);
                var param5 = new SqlParameter("@ProcessID", objProcess[x].ProcessID);
                var param9 = new SqlParameter("@SituationID", "1");
                objProduct = _context.ProductMasterData.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping @UserID,@ProcessID,@SituationID", Parm8, param5, param9).ToList();

                foreach (var item1 in objProduct)
                {

                    //  obj.Clear();
                    obj.Add(new Product { ProductName = item1.ProductName, ProductID = item1.ProductID, Mapped = item1.Mapped });
                }
                ProcessProduct.Add(new ProcessProduct { Product = obj, ProcessName = objProcess[x].ProcessName, ProcessID = objProcess[x].ProcessID });
            }

            var param10 = new SqlParameter("@UserID", users.UserID);
            var param11 = new SqlParameter("@SituationID", "2");
            var Page = _context.AllMenuShow.FromSqlRaw("exec usp_select_Admin_MUserProcessProductMapping_Changes @UserID,@ProcessID,@SituationID", param10, param2, param11).ToList();            
            var distncparent = Page.Where(i => i.ParentMenuID == 0).Distinct().ToList();
            List<AllUserMenu> FinaluserMenu = new List<AllUserMenu>();
            AllUserMenu userMenu = new AllUserMenu();

            for (int i = 0; i < distncparent.Count; i++)
            {

                List<AllMenuShow> menu = new List<AllMenuShow>();
                List<AllMenuShow> chilMenu = new List<AllMenuShow>();
                chilMenu = Page.Where(z => z.ParentMenuID == distncparent[i].MenuID).ToList();
                for (int j = 0; j < chilMenu.Count; j++)
                {
                    menu.Add(new AllMenuShow
                    {
                        MenuID = chilMenu[j].MenuID,
                        MenuName = chilMenu[j].MenuName,
                        Collapse = chilMenu[j].Collapse,
                        Icon = chilMenu[j].Icon,
                        ParentMenuID = distncparent[i].ParentMenuID,
                        Route = chilMenu[j].Route,
                        PageID = chilMenu[j].PageID,
                        Mapped = chilMenu[j].Mapped
                    });
                }

                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.MenuName = distncparent[i].MenuName;
                userMenu.Icon = distncparent[i].Icon;
                userMenu.Collapse = distncparent[i].Collapse;
                userMenu.Route = distncparent[i].Route;
                userMenu.ChildMenu = menu;

                FinaluserMenu.Add(new AllUserMenu
                {
                    Collapse = distncparent[i].Collapse,
                    MenuName = distncparent[i].MenuName,
                    Icon = distncparent[i].Icon,
                    Route = distncparent[i].Route,
                    ChildMenu = menu
                });

            }

            if (ProcessProduct.Count > 0)
            {
                string Data =
                        "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(ProcessProduct) + "," +
                          "\"Page\":" + Newtonsoft.Json.JsonConvert.SerializeObject(FinaluserMenu) + "}";
                return Ok(Data);
            }
            else
            {
                return NotFound();
            }


        }

        // GET: api/UsersMaster
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Users>>> GetAdmin_Muser()
        //{
        //    return await _context.Admin_Muser.ToListAsync();
        //}

        [Route("[action]")]
        [HttpGet]
        public IActionResult GetAdmin_Muser()
        {

            var data = _context.GetUserDetailAdmin.FromSqlRaw("exec usp_select_Admin_MUser").ToList();
            return Ok(data);

        }




        // GET: api/UsersMaster/5
        //[HttpGet("{id}")]
        //public async Task<ActionResult<Users>> GetUsers(int id)
        //{
        //    var users = await _context.Admin_Muser.FindAsync(id);

        //    if (users == null)
        //    {
        //        return NotFound();
        //    }

        //    return users;
        //}

        // PUT: api/UsersMaster/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutUsers(int id, Users users)
        //{
        //    if (id != users.UserID)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(users).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!UsersExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/UsersMaster
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        //[HttpPost]
        //public async Task<ActionResult<Users>> PostUsers(Users users)
        //{
        //    _context.Admin_Muser.Add(users);
        //    await _context.SaveChangesAsync();

        //    return CreatedAtAction("GetUsers", new { id = users.UserID }, users);
        //}

        // DELETE: api/UsersMaster/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Users>> DeleteUsers(int id)
        //{
        //    var users = await _context.Admin_Muser.FindAsync(id);
        //    if (users == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Admin_Muser.Remove(users);
        //    await _context.SaveChangesAsync();

        //    return users;
        //}

        private bool UsersExists(int id)
        {
            return _context.Admin_Muser.Any(e => e.UserID == id);
        }

        [Route("[action]")]
        [HttpGet]
        public IActionResult UserDeactivate(int userid)
        {
            var deactiveuser = _context.Admin_Muser.Where(u => u.UserID == userid).FirstOrDefault();
            bool status = deactiveuser.IsActive;
            if (status)
            {
                deactiveuser.IsActive = false;
            }
            else
            {
                deactiveuser.IsActive = true;
            }

            _context.SaveChanges();
            return Ok();
        }
    }
}
