﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;
using Labguru_Angular_API.Models;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductGroupMasterController : ControllerBase
    {
        private readonly SqlDbContext _context;

        public ProductGroupMasterController(SqlDbContext context)
        {
            _context = context;
        }

        // GET: api/ProductGroupMaster
        [HttpGet]
        public List<SelectProductGroupMaster> GetAdmin_MProductGroup(int SituationID)
        {
            var param1 = new SqlParameter("@SituationID", SituationID);
            return _context.SelectProductGroupMaster.FromSqlRaw("exec usp_select_Admin_MProductGroup @SituationID ", param1).ToList();

        }
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<ProductGroupMaster>>> GetAdmin_MProductGroupMaster()
        //{
        //    return await _context.Admin_MProductGroupMaster.ToListAsync();
        //}

        // GET: api/ProductGroupMaster/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductGroupMaster>> GetProductGroupMaster(int id)
        {
            var productGroupMaster = await _context.Admin_MProductGroupMaster.FindAsync(id);

            if (productGroupMaster == null)
            {
                return NotFound();
            }

            return productGroupMaster;
        }

        // PUT: api/ProductGroupMaster/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductGroupMaster(int id, ProductGroupMaster productGroupMaster)
        {
            Error obj = new Error();

            obj.message = "Product Group Updated Successfully";
            obj.error = "0";


            if (id != productGroupMaster.ProductGroupID)
            {
                return BadRequest();
            }

            try
            {
                _context.Update(productGroupMaster);
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException ex)
            {
                if (!ProductGroupMasterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    obj = Models.Common.CommonException(ex);
                }
            }

            string Data =
                    "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                    "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
        }

        // POST: api/ProductGroupMaster
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<ProductGroupMaster>> PostProductGroupMaster(ProductGroupMaster productGroupMaster)
        {
            Error obj = new Error();

            obj.message = "ProductGroup Added Successfully";
            obj.error = "0";

            _context.Admin_MProductGroupMaster.Add(productGroupMaster);
            try
            {

                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException ex)
            {
                obj = Models.Common.CommonException(ex);

            }

            string Data =
                     "{\"success\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.message) + ", " +
                     "\"error\":" + Newtonsoft.Json.JsonConvert.SerializeObject(obj.error) + "}";
            return Ok(Newtonsoft.Json.JsonConvert.SerializeObject((Data)));
        }

        // DELETE: api/ProductGroupMaster/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductGroupMaster>> DeleteProductGroupMaster(int id)
        {
            var productGroupMaster = await _context.Admin_MProductGroupMaster.FindAsync(id);
            if (productGroupMaster == null)
            {
                return NotFound();
            }

            _context.Admin_MProductGroupMaster.Remove(productGroupMaster);
            await _context.SaveChangesAsync();

            return productGroupMaster;
        }

        private bool ProductGroupMasterExists(int id)
        {
            return _context.Admin_MProductGroupMaster.Any(e => e.ProductGroupID == id);
        }
    }
}
