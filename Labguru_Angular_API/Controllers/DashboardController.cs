﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Labguru_Angular_API.DbContexts;
using Labguru_Angular_DTO;
using Microsoft.Data.SqlClient;

namespace Labguru_Angular_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly SqlDbContext _context;



        [Route("[action]")]
        [HttpPost]
        public IActionResult GetDashboard(DashboardInputDTO dashboard)
        {


            List<CommonOutPut> objProduct = new List<CommonOutPut>();
            var userid = new SqlParameter("@UserID", dashboard.UserID);
            var processid = new SqlParameter("@ProcessID", dashboard.ProcessID);
            var situationid = new SqlParameter("@SituationID", 4);
            objProduct = _context.Common.FromSqlRaw("exec usp_Select_Common_DLL @UserID, @ProcessID,@SituationID", userid, processid, situationid).ToList();
            List<Dashboard> Dashboard = new List<Dashboard>();
            for (int x = 0; x < objProduct.Count; x++)
            {
                List<DashboardOutputDTO> objDashboardOutputDTO = new List<DashboardOutputDTO>();
                List<DashboardOutputDTO> obj = new List<DashboardOutputDTO>();

                var Parm8 = new SqlParameter("@UserID", dashboard.UserID);
                var param5 = new SqlParameter("@ProcessID", dashboard.ProcessID);
                var param9 = new SqlParameter("@ProductID", objProduct[x].ID);
                //var situationid1 = new SqlParameter("@SituationID", 0);

                _context.Database.SetCommandTimeout(180);
                objDashboardOutputDTO = _context.DashboardOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry_Dashboard @UserID,@ProcessID,@ProductID", Parm8, param5, param9).ToList();

                    Dashboard.Add(new Dashboard { DashboardOutputDTO = objDashboardOutputDTO, ProductID = objProduct[x].ID, Product = objProduct[x].Description });
            }

            if (Dashboard.Count > 0)
            {
                string Data =
                        "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(Dashboard) + "}";
                return Ok(Data);
            }
            else
            {
                return NotFound();
            }


        }


        public DashboardController(SqlDbContext context)
        {
            _context = context;
        }

        // GET: api/Dashboard
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dashboard>>> GetDashboards()
        {
            return await _context.Dashboards.ToListAsync();
        }

        // GET: api/Dashboard/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Dashboard>> GetDashboard(int id)
        {
            var dashboard = await _context.Dashboards.FindAsync(id);

            if (dashboard == null)
            {
                return NotFound();
            }

            return dashboard;
        }

        // PUT: api/Dashboard/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDashboard(int id, Dashboard dashboard)
        {
            if (id != dashboard.ProductID)
            {
                return BadRequest();
            }

            _context.Entry(dashboard).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DashboardExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Dashboard
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<Dashboard>> PostDashboard(Dashboard dashboard)
        {
            _context.Dashboards.Add(dashboard);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDashboard", new { id = dashboard.ProductID }, dashboard);
        }

        // DELETE: api/Dashboard/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Dashboard>> DeleteDashboard(int id)
        {
            var dashboard = await _context.Dashboards.FindAsync(id);
            if (dashboard == null)
            {
                return NotFound();
            }

            _context.Dashboards.Remove(dashboard);
            await _context.SaveChangesAsync();

            return dashboard;
        }

        private bool DashboardExists(int id)
        {
            return _context.Dashboards.Any(e => e.ProductID == id);
        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult SearchFileEntry(FileEntrySearchInputDTO file)
        {
            var productid = new SqlParameter("@ProductID", file.ProductID);
            var impressionno = new SqlParameter("@ImpressionNo", file.ImpressionNo);
            if (impressionno.Value == null)
            {
                impressionno.Value = "";
            }
            var statusid = new SqlParameter("@StatusID", file.StatusID);
            if(statusid.Value==null)
            {
                statusid.Value = 0;
            }
            var processid = new SqlParameter("@ProcessID", file.ProcessID);
            var transactionnumber = new SqlParameter("@TransactionNunber", file.TransactionNumber);
            var situationid = new SqlParameter("@SituationID", 0);
            var userId = new SqlParameter("@UserID", file.UserID);
            situationid.Value = 0;
            var data = _context.FileEntrySearchOutPutDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry @ProductID, @ImpressionNo,@StatusID,@ProcessID,@TransactionNunber,@SituationID,@UserID",
                productid, impressionno, statusid, processid, transactionnumber, situationid, userId).ToList();

            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);



        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult DownloadFileEntry(DownloadFileDetails file)
        {
            var productid = new SqlParameter("@ProductID", 0);
            productid.Value = 0;
            var impressionno = new SqlParameter("@ImpressionNo", null);
            if (impressionno.Value == null)
            {
                impressionno.Value = "";
            }
            var statusid = new SqlParameter("@StatusID", 0);
            statusid.Value = 0;
            var processid = new SqlParameter("@ProcessID", file.ProcessID);
         //   processid.Value = 0;
            var transactionnumber = new SqlParameter("@TransactionNunber", file.TransactionNumber);
            var situationid = new SqlParameter("@SituationID", 0);
            situationid.Value = 1;
            var data = _context.FileEntryFileOutPutDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry @ProductID, @ImpressionNo,@StatusID,@ProcessID,@TransactionNunber,@SituationID",
                productid, impressionno, statusid, processid, transactionnumber, situationid).ToList();


            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);

        }
        [Route("[action]")]
        [HttpPost]
        public IActionResult GetSearchCase(SearchCaseInputDTO caseinput)
        {
            var productid = new SqlParameter("@ProductID", 0);
            productid.Value = 0;
            var impressionno = new SqlParameter("@ImpressionNo", caseinput.ImpressionNo);
            if (impressionno.Value == null)
            {
                impressionno.Value = "";
            }
            var statusid = new SqlParameter("@StatusID", 0);
            statusid.Value = 0;
            var processid = new SqlParameter("@ProcessID", 0);
            processid.Value = 0;
            var transactionnumber = new SqlParameter("@TransactionNunber", null);
            if (transactionnumber.Value == null)
            {
                transactionnumber.Value = "";
            }
            var situationid = new SqlParameter("@SituationID", 0);
            situationid.Value = 2;
            var data = _context.SearchCaseOutPutDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry @ProductID, @ImpressionNo,@StatusID,@ProcessID,@TransactionNunber,@SituationID",
                productid, impressionno, statusid, processid, transactionnumber, situationid).ToList();


            string Data =
                    "{\"Data\":" + Newtonsoft.Json.JsonConvert.SerializeObject(data) + "}";
            return Ok(Data);

        }

        [Route("[action]")]
        [HttpPost]
        public IActionResult GetStatusDashboard(DashboardInputDTO dashboard)
        {
            var UserID = new SqlParameter("@UserID", dashboard.UserID);
            var ProcessID = new SqlParameter("@ProcessID", dashboard.ProcessID);
            var ProductID = new SqlParameter("@ProductID", dashboard.ProductID);
            var SituationID = new SqlParameter("@SituationID", 1);//open status

            var openStatusDetails = _context.DashboardStatusOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry_Dashboard @UserID,@ProcessID,@ProductID,@SituationID", UserID, ProcessID, ProductID, SituationID).ToList();
            _context.Dispose();
            SqlDbContext _context2 = new SqlDbContext();

            var SituationID1 = new SqlParameter("@SituationID", 2);//in Process status
            var inProcessStatusDetails = _context2.DashboardStatusOutputDTO.FromSqlRaw("exec USP_SELECT_PP_TFileEntry_Dashboard @UserID,@ProcessID,@ProductID,@SituationID", UserID, ProcessID, ProductID, SituationID1).ToList();
            _context2.Dispose();
            string Data =
                              "{\"OpenStatusDetails\":" + Newtonsoft.Json.JsonConvert.SerializeObject(openStatusDetails) + "," +
                           "\"InProcessStatusDetails\":" + Newtonsoft.Json.JsonConvert.SerializeObject(inProcessStatusDetails) + "}";
            ;
            return Ok(Data);
        }

    }
}
